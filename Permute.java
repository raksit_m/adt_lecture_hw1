/**
 *
 * @author paruj
 */
import java.util.*;

public class Permute {

	public static ArrayList<String> permute(String s) {
		ArrayList<String> as = new ArrayList<String>();
		String sss;
		if (s.length() == 1) { as.add(s); return as; }
		ArrayList<String> ass = permute(s.substring(1));
		for (String ss : ass) {

			for (int i=0; i<ss.length()+1; i++) {
				sss = new String();
				sss += ss.substring(0, i) + s.substring(0, 1) + ss.substring(i, ss.length());
				as.add(sss);
			}
		}
		return as;
	}
	
	/**
	 * PermuteK returns list of permuted String with size k, with the following processes:
	 * 
	 * 1.) Get the list of permuted String with the same size as String s.
	 * 2.) Modify each element size of the array to be k.
	 * 3.) Convert to set in order to removing duplicated elements.
	 * 4.) Convert back to ArrayList and return.
	 * 
	 * @param s String which will be permuted.
	 * @param k size (length) of elements.
	 * @return list of permuted String with size k.
	 * @throws IllegalArgumentException, in case that k is out of length.
	 * @author Raksit Mantanacharu 5710546402
	 */
	
	public static ArrayList<String> permuteK(String s, int k) {
		
		if(k > s.length() || k < 0) throw new IllegalArgumentException("Invalid input");
		
		ArrayList<String> as = permute(s);
		
		for(int i = 0; i < as.size(); i++) {
			as.set(i, as.get(i).substring(0, k));
		}
		
		Set<String> set = new HashSet<String>(as);
		
		as = new ArrayList<String>(set);
		
		return as;
		
	}

	public static void main(String[] args) {
		ArrayList<String> as = permuteK("abcd", 2);

		Collections.sort(as);
		for (int i=0; i<as.size(); i++)
			System.out.println(as.get(i));
		System.out.println("Permutation size is " + as.size());
		/*
		for (String s: as)
			System.out.println(s);
		 */
	}
}
