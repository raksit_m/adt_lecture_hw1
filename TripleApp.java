import java.util.ArrayList;
import java.util.List;

/**
 * Combination using Triple object and memorization class.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class TripleApp {

	private static List<Triple> cached = new ArrayList<Triple>();

	/**
	 * Combination = n!/(r!)(n-r)! using memorization.
	 * 
	 * - Program will store value in cached, if the value has already be in the cached then
	 * it will use memorized one.
	 * 
	 * - Otherwise, it will create new one and memorize to the cached.
	 * 
	 * @param n as formula above.
	 * @param r as formula above.
	 * @return value of combinations (Triple's attribute)
	 */
	public static int combs(int n, int r) {

		// Creating new object for recursion.
		Triple triple = new Triple(n, r, 0);
		
		// Base case.
		
		if (r == 0 || r == n) {
			
			triple.setValue(1);
			
			return triple.getValue();
		}

		// contains method uses overridden equals() from Triple class.
		
		if(cached.contains(triple)) {

			// using memorized one if it does exist.

			triple.setValue(cached.get(cached.indexOf(triple)).getValue());
			
			return triple.getValue();

		}
		
		// Otherwise, set the new value and add it to cached.

		triple.setValue(combs(n-1,r) + combs(n-1,r-1));

		cached.add(triple);

		return triple.getValue();

	}


	public static void main(String[] args) {

		System.out.println(combs(5, 3));

		System.out.println(combs(6, 4));
		
		System.out.println(combs(10, 8));
	}
}
