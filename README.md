## ADT Lecture Homework 1: Recursion ##
#
#
#
#
#
### Raksit Mantanacharu 5710546402 ###
#
#
#
#
#

https://bitbucket.org/raksit_m/adt_lecture_hw1/
#
#
#
#
#

These files include the Java source codes in 5 problems:

#
#
#

**1.) Rec.java** (for Problem 1)

#

**2.) Permute.java** (for Problem 2)

	   PermuteK returns list of permuted String with size k, with the following processes:

	   1.) Get the list of permuted String with the same size as String s.

	   2.) Modify each element size of the array to be k.

	   3.) Convert to set in order to removing duplicated elements.

	   4.) Convert back to ArrayList and return.

#

**3.) Pascal.java** (for Problem 3)

	   Create nth line of Pascal triangle by calculation.

	   Note that from 1 to (line-1) index, the value is come from the summation of two adjacent previous line.

       See the example below (4 lines)

               1                                     1
   
           1       1                            1         1
                                 =            
        1    (1+1)    1                     1        2         1

    1  (1+2)     (1+2)    1             1       3         3       1


#

**4.) Triple.java** and **TripleApp.java** (for Problem 4)


	   Combination = n!/(r!)(n-r)! using memorization.
	   
	   - Program will store value in cached, if the value has already be in the cached then it will use memorized one.
	 
	   - Otherwise, it will create new one and memorize to the cached.

       - Using contains() method which requires overridden equals() from Triple class.
#

**5.) Maze.java** (for Problem 5)

	   Solving maze by using these above methods with the following processes:
	   
	   1.) Base case: check whether that coordinate is finish or not, if it is then maze solved.
	   
	   2.) if that location is inside the maze, it has no obstacle and it's not finish, then it would be the path to finish.
	 
	   3.) Recursion Step: check all 4 directions (north, east, south, west) that whether it would be the path to finish or 

       not, if it is then check again and so on...
	   
	   4.) Otherwise, return false to indicate that the path is not the way to finish.