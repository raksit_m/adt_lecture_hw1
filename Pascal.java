import java.util.Scanner;

/**
 * Pascal triangle with input lines methods using recursion.
 * 
 * @author Raksit Mantanacharu 5710546402
 *
 */

public class Pascal {

	/**
	 * Create nth line of Pascal triangle by calculation.
	 * Note that from 1 to (n-1) index, the value is come from 
	 * the summation of two adjacent previous line.
	 * 
	 * @param num (nth line)
	 * @return Arrays containing number in that line.
	 * @throws IllegalArgumentException if input line is invalid.
	 * 
	 */
	public static int[] pascalLine(int num) {

		if(num < 1) throw new IllegalArgumentException("Invalid input");

		else if (num == 1) return new int[] {1};

		int[] current = new int[num];

		int[] previous = pascalLine(num-1);
		
		current[0] = 1; // leftmost column.
		
		current[num-1] = 1; // rightmost column.

		for(int i = 1; i < num - 1; i++) {
			current[i] = previous[i-1] + previous[i]; // summation of two adjacent previous line.
		}

		return current;
	}
	
	/**
	 * Print Pascal triangle with num lines.
	 * @param num of lines
	 */
	public static void printPascal(int num) {
		
		for(int i = 0; i < num; i++) {
			
			int[] pascal = pascalLine(i+1);
			
			for(int j = 0; j < i + 1; j++) {
				
				System.out.print(pascal[j] + " ");
			}
			
			System.out.println();
		}
	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int num = scanner.nextInt();
		printPascal(num);
	}
}
