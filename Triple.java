/**
 * Triple object which store combinations value by memorization.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class Triple {

	private int n, r, v;

	public Triple(int n, int r, int v) {

		this.n = n;
		this.r = r;
		this.v = v;

	}
	
	@Override
	
	/**
	 * Overridden equals() method which is used for contains() method
	 * to find the value matching n and r.
	 */
	public boolean equals(Object obj) {
		
		if(obj == null) return false;
		
		if(this.getClass() != obj.getClass()) return false;
		
		Triple triple = (Triple) obj;
		
		if(this.getN() == triple.getN() && this.getR() == triple.getR()) return true;
		
		return false;
	}
	
	public int getN() { return this.n; }
	
	public int getR() { return this.r; }
	
	public int getValue() { return this.v; }
	
	public void setValue(int v) { this.v = v;}

}
